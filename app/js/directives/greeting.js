'use strict';

eventsApp
  .directive('greeting', function(){
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      //priority: 1,
      template: "<div><button class='btn' ng-click='sayHello()'>Say Hello</button><div ng-transclude></div></div>",
      controller: 'GreetingController'
      //controller: "@",
      //name: "ctrl"
    };
  })
  .directive('finnish', function() {
    return {
      restrict: 'A',
      require: '^greeting',
      //priority: 3,
      link: function(scope, element, attrs, controller) {
        controller.addGreeting('hei');
      }
    };
  })


  .directive('hindi', function() {
    return {
      restrict: 'A',
      require: '^greeting',
      //priority: 2,
      link: function(scope, element, attrs, controller) {
        controller.addGreeting('Namaste');
      }
    };
  });

eventsApp.controller('GreetingController', function GreetingController($scope) {
  var greetings = ['hello'];
  $scope.sayHello = function() {
    console.log(greetings);
  };
  this.addGreeting = function(greeting) {
    greetings.push(greeting);
  };
});