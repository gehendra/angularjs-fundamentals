'use strict';

eventsApp.factory('$exceptionController', function() {
    return function(exception) {
      console.log("exception handled: " + exception.message);
    };
  }
);