'use strict';

eventsApp.controller("EventController", function EventController($scope, eventData, $routeParams, $route) {
  $scope.sortorder = 'name';
  
  $scope.event = $route.current.locals.event;

  $scope.upVoteSession = function(session){
    session.upVoteCount++;
  };
  $scope.downVoteSession = function(session){
    if (session.upVoteCount >= 0){
      session.upVoteCount--;
    }
  };
  $scope.scrollToSession = function() {
    $anchorScroll();
  };

  $scope.reload = function() {
    $route.reload();
  }
});